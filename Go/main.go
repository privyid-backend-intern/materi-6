package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

type Tasks struct {
	ID         string `json:"id"`
	TaskName   string `json:"task_name"`
	TaskDetail string `json:"task_detail"`
	Date       string `json:"date"`
}

var tasks []Tasks

func allTasks() []Tasks {
	task := Tasks{
		ID:         "1",
		TaskName:   "New project",
		TaskDetail: "You must lead the project and finish it",
		Date:       "2022-01-22",
	}
	tasks = append(tasks, task)

	task1 := Tasks{
		ID:         "2",
		TaskName:   "Power project",
		TaskDetail: "We need to hire more stuffs before the deadline",
		Date:       "2022-01-22",
	}
	tasks = append(tasks, task1)

	return tasks
}

//func homePage(rw http.ResponseWriter, r *http.Request) {
//	fmt.Println("Homepage")
//}

func getTasks(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	fmt.Println(r)
	json.NewEncoder(rw).Encode(tasks)
}

func getTask(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	taskId := mux.Vars(r)
	fmt.Println(taskId["id"])
	flag := false
	for i := 0; i < len(tasks); i++ {
		if taskId["id"] == tasks[i].ID {
			json.NewEncoder(rw).Encode(tasks[i])
			flag = true
			break
		}
	}
	if flag == false {
		json.NewEncoder(rw).Encode(map[string]string{"status": "Error"})
	}
}

func createTask(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	var task Tasks
	_ = json.NewDecoder(r.Body).Decode(&task) // parse request body into task
	task.ID = strconv.Itoa(rand.Intn(1000))   // generate random id
	task.Date = time.Now().Format("2006-01-02")
	tasks = append(tasks, task)
	json.NewEncoder(rw).Encode(task)
}

func deleteTask(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	flag := false
	for index, item := range tasks {
		if item.ID == params["id"] {
			tasks = append(tasks[:index], tasks[index+1:]...) // spread operator
			flag = true
			json.NewEncoder(rw).Encode(map[string]string{"status": "Success"})
			return
		}
	}
	if flag == false {
		json.NewEncoder(rw).Encode(map[string]string{"status": "Error"})
	}
}

func updateTask(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	flag := false
	for index, item := range tasks {
		if item.ID == params["id"] {
			tasks = append(tasks[:index], tasks[index+1:]...) // spread operator
			var task Tasks
			_ = json.NewDecoder(r.Body).Decode(&task)
			task.ID = params["id"]
			task.Date = time.Now().Format("2006-01-02")
			tasks = append(tasks, task)
			flag = true
			json.NewEncoder(rw).Encode(task)
			return
		}
	}
	if flag == false {
		json.NewEncoder(rw).Encode(map[string]string{"status": "Error"})
	}
}

func handleRoutes() {
	router := mux.NewRouter()

	//router.HandleFunc("/", homePage).Methods("GET")
	router.HandleFunc("/getTasks", getTasks).Methods("GET")
	router.HandleFunc("/getTask/{id}", getTask).Methods("GET")
	router.HandleFunc("/createTask", createTask).Methods("POST")
	router.HandleFunc("/deleteTask/{id}", deleteTask).Methods("DELETE")
	router.HandleFunc("/updateTask/{id}", updateTask).Methods("PUT")

	log.Fatal(http.ListenAndServe(":8000", router))
}
func main() {
	allTasks()
	fmt.Println("Hello")
	handleRoutes()

}
