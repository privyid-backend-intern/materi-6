class AppConstants {
  static const String BASE_URL = "http://localhost:8000";
  static const String GET_TASKS = "/getTasks";
  static const String GET_TASK = "/getTask/";
  static const String POST_TASK = "/createTask";
  static const String UPDATE_TASK = "/updateTask/";
  static const String DELETE_TASK = "/deleteTask/";
}